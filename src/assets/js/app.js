document.addEventListener("DOMContentLoaded", CALENDAR().init);

function CALENDAR () {
	var d = document.querySelector('body'),
			prew = d.querySelector('#js-prew'),
			next = d.querySelector('#js-next'),
			label = document.querySelector('#js-label'),
			today = d.querySelector('#js-to-today'),
			addNew = d.querySelector('#js-add'),
			refresh = d.querySelector('#js-refresh'),
			submit = d.querySelector('#js-eventAdd'),
			cancel = d.querySelector('#js-eventCancel'),
			searchBtn = d.querySelector('#js-search-ico'),
			searchField = d.querySelector('#js-search'),
			formBlock = d.querySelector('.event-form-block'),
			months = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
			weekDays = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота", "Воскресенье"];


	// Event listeners and CALENDAR initiation
	//----------------------------------------

	function init() {
		prew.addEventListener ('click', function(){ switchMonth(false); });
		next.addEventListener ('click', function(){ switchMonth(true); });
		today.addEventListener ('click', function(){
			switchMonth(null, new Date().getMonth(), new Date().getFullYear());
		});
		addNew.addEventListener ('click', function(){ toggleActive(formBlock); });
		cancel.addEventListener ('click', function(e){
			e.preventDefault();
			toggleActive(formBlock);
		});
		submit.addEventListener ('click', function(e){
			e.preventDefault();
			eventCreate();
		});
		refresh.addEventListener ('click', function(){
			months.forEach(function(item, i){
				if( item == label.textContent.split(' ')[0]) {
					switchMonth(null, i, +label.textContent.split(' ')[1]);
				}
			});
		});
		searchBtn.addEventListener ('click', function(){ search(); });
		searchField.addEventListener("keydown", function(event) {
			if (event.keyCode == 13) { search(); }
		});

		// Setup today's initial date
		switchMonth(null, new Date().getMonth(), new Date().getFullYear());
	}


	// Toggle is-active
	//-----------------

	function toggleActive (elem) {
		if (/\bis-active\b/.test(elem.className)) {
			elem.className = elem.className.replace(/\b is-active\b/g,'');
		} else {
			elem.className += ' is-active';
		}
	}


	// Switching month function
	//-------------------------

	function switchMonth(next, month, year) {
		var calendar,
				label = document.querySelector('#js-label'),
				currMonth = label.textContent.trim().split(' ')[0],
				currYear = +label.textContent.trim().split(' ')[1],
				calFrame = document.querySelector('#js-cal-frame'),
				currFrame = document.querySelector('#js-cal-frame__curr'),
				frameNode = document.createElement('table');

		// Calendar frame preset
		frameNode.className = 'cal-frame__curr';
		frameNode.id = 'js-cal-frame__curr';

		if (!month) {
			if (next) {
				if (currMonth == "Декабрь") { month = 0; }  // if month's index = 11
				else { month = months.indexOf(currMonth) + 1; }  // default switching
			} else {
				if (currMonth == "Январь") { month = 11; }  // if month's index = 0
				else { month = months.indexOf(currMonth) - 1; }  // default switching
			}
		}

		if (!year) {
			year = currYear;
			if (next && month === 0) { year++; } //if switched to Jan from Dec
			else if (!next && month === 11) { year--; } //if switched to Dec from Jan
		}

		calendar =  createCal(year, month);  //{calendar-innerHTML(string), label, startDay-dayOfWeek}
		currFrame.id = "";
		currFrame.className = "cal-frame__temp";
		frameNode.innerHTML = calendar.calendar;

		// Setup calendar Node today's day
		if (month === new Date().getMonth()) {
			frameNode.querySelectorAll('td')[new Date().getDate() + calendar.startDay - 1].className += ' today';
		}

		calFrame.appendChild(frameNode);
		fadeOut(currFrame);
		label.textContent = calendar.label;
	}


	// Fading out function
	//--------------------

	function fadeOut (elem, time) {
		var timer;

		if (time === undefined) { time = 500; }
		if (time === 0) { elem.style.opacity = 0; }
		else if (time > 0) {
			elem.style.opacity = elem.style.opacity || 1;
			timer = setInterval(function(){
				elem.style.opacity -= 0.01;
				if (elem.style.opacity <= 0) {
					clearTimeout(timer);
					elem.parentNode.removeChild(elem);
				}
			}, time / 100);
		}
	}

	// Event constructing
	//-------------------

	function eventCreate () {
		var form = document.querySelector('#js-event-form'),
				eventDate = form.elements.eventDate,
				date = eventDate.value.split(/[\.\-\,\s]/),
				eventHeadline = form.elements.eventHeadline,
				eventDescript = form.elements.eventMembers,
				event = {};

		if (date[0] && date[1] && date[2] && eventHeadline.value) {
			event = {
				day: +date[2] - 1,
				headline: eventHeadline.value,
				descript: eventDescript.value
			};
			toggleActive(formBlock);
			eventDate.value = eventHeadline.value = eventDescript.value = '';
			createCal(+date[0], +date[1] - 1, event);
		} else if (!date[0] || !date[1] || !date[2]) {
			alert('Введите дату.');
		} else if (!eventHeadline.value) {
			alert('Введите событие.');
		}
	}


	// Creating calendar Node function
	//--------------------------------

	function createCal (year, month, event) {
		var day = 1, i, j, haveDays = true,
				startDay = new Date(year, month, day).getDay(),
				daysInMonths = [31, (((year%4===0)&&(year%100!==0))||(year%400===0)) ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
				daysInPrewMonth = daysInMonths.slice(-1).concat(daysInMonths.slice(0,-1)),
				calEvents = [],
				calendar = [];

		// Shift week days
		if (startDay === 0) {startDay = 6;}
		else {startDay--;}

		// Inserting event
		if (event) {
			if (createCal.cache[year])
				if (createCal.cache[year][month])
					if (createCal.cache[year][month])
						if (createCal.cache[year][month].calEvents)
							calEvents[event.day] = createCal.cache[year][month].calEvents[event.day] || [];

			if (!calEvents[event.day]) {
				calEvents[event.day] = [];
			}
			calEvents[event.day].push(event);
		}

		// Calendar cache
		if (createCal.cache[year]) {
			if (createCal.cache[year][month]) {
				// Inserting Event
				if (event) {
					createCal.cache[year][month].calendar = eventInsert(event, createCal.cache[year][month].calendar, startDay);
				}
				return createCal.cache[year][month];
			}
		} else {
			createCal.cache[year] = {};
		}

		// Creating calendar weeks
		for (i = 0; haveDays; i++) {
			calendar[i] = [];
			for (j = 0; j < 7; j++) {
				if (i === 0) {
					if (j < startDay) { // Setting prew month days
						calendar[i][j] = daysInPrewMonth[month] - startDay + j + 1;
					} else {
						calendar[i][j] = day++;
					}
					calendar[i][j] = weekDays[j] + ', ' + calendar[i][j];
				} else if (day <= daysInMonths[month]) {
					calendar[i][j] = day++;
				} else if (day > daysInMonths[month] && i < 5) {
					calendar[i][j] = day++ - daysInMonths[month];
				} else {
					calendar[i][j] = "";
					haveDays = false;
				}
				if (day > daysInMonths[month]) {haveDays = false;}
			}
		}

		// Merging 6th week with 5th
		if (calendar[5]) {
			for (i = 0; i < calendar[5].length; i++) {
				if (calendar[5][i] !== "") {
					calendar[4][i] = '<span class="double-date-curr">' + calendar[4][i] + '</span><span class="double-date-next">' + calendar[5][i] + '</span>';
				}
			}
			calendar = calendar.slice(0, 5);
		}

		// Calendar to string
		for (i = 0; i < calendar.length; i++) {
			calendar[i] = "<tr><td>" + calendar[i].join("</td><td>") + "</td></tr>";
		}
		calendar = calendar.join("");

		// Inserting Event
		if (event) {
			calendar = eventInsert(event, calendar, startDay);
		}
		// Creating Cached Calendar Object
		createCal.cache[year][month] = { calendar: calendar, label: months[month] + " " + year, startDay: startDay, calEvents: calEvents };

		//return from cache
		return createCal.cache[year][month];
	}


	// Function for events insertion
	//------------------------------

	function eventInsert (event, calendar, startDay) {
		var i, j, k;

		if (event.day < 7 - startDay) {
			i = new RegExp('\>\\s?'+ (weekDays[event.day + startDay]) + ', ' + (event.day + 1) +'\\s?\\<','i');
		} else {
			i = new RegExp('\>\\s?\\b'+ (event.day + 1) +'\\s?\\<','i');
		}
		k = calendar.search(i);
		j = calendar.slice(0, k);
		j +=  ' class="cal-event"';
		i = calendar.slice(k).search((event.day + 1));
		i += (event.day + 1).toString().length + k;
		j += calendar.slice(k, i);
		j += '<span class="cal-event__headline">' + event.headline + '</span>';
		if (event.descript) {
			j += '<span class="cal-event__members">' + event.descript + '</span>';
		}
		j += calendar.slice(i);
		return j;
	}


	// Event/date searching (well, mostly date)
	//-----------------------------------------

	function search () {
		var date,
				content = searchField.value;

		if (content) {date = content.split(/[\.\-\,\s]/);}
		else {alarm('Введите поисковой запрос');}

		if ((date.length !== 3) || (+date[1] > 12) || (+date[1] < 0) ||
				(!(date[0].length === 4 && +date[2] <= 31 && +date[2] > 0) &&
				 !(date[2].length === 4 && +date[0] <= 31 && +date[0] > 0)) ||
				!+date[0] || !+date[1] || !+date[2]) {
			alert('Пардоньте, но пока ищем только дату в формате "дд мм гггг" или "гггг мм дд"');
		} else if (date[0].length == 2) {
			switchMonth(null, date[1] - 1, +date[2]);
		} else {
			switchMonth(null, date[2] - 1, +date[0]);
		}
		searchField.value = '';
	}

	createCal.cache = {};
	return {
		init: init,
		switchMonth: switchMonth,
		createCal: createCal
	};
}
